============
Introducción
============

Mestizo es un andamio [#r1]_ desacoplado de propósito general potenciado por PHP.

Mestizo es un andamio ligero, por sí mismo provee las clases necesarias para construir cualquier tipo de aplicaciones Web, particularmente bajo el patrón MVC. Mestizo ha sido diseñado con énfsis en proveer una estructura para organizar las aplicaciones que potencie y en el componente *Controlador* del patrón MVC. También se provee un soporte ligero para el renderizado de HTML, en tanto consideramos que la Vista tiende a ser la parte más personalizada en aplicaciones Web tradicionales y a cambiar completamente cuando se exponen API para Servicios Web. Mestizo no provee ningún soporte para el componente de Modelo, relegando este a paquetes externos, recomendamos Alpaca, pero con Mestizo es posible y extremadamente sencillo utilizar otros ORM, como ReadBeanPHP o Doctrine, o bien, seguir un patrón más cercano a PDO con Tuza e incluso prescindir de dichas capas e implementar el Modelo direcamente sobre PDO.

Con Mestizo es sencillo construir aplicaciones tradicionales, como CMS, así como complejas aplicaciones complejas Web que expongan API RESTful, como Redes Sociales, CRM, ERP, etc.

Arquitectura
============

La arquitectura de Mestizo es secilla, se divide en cinco componentes claramente definidos, fáciles de aprender e implementar.

El componente ``application``
-----------------------------

Por hacer

El componente ``controller``
----------------------------

Por hacer

El componente ``http``
----------------------

Por hacer

El componente ``user``
----------------------

Por hacer

El componente ``Router``
------------------------

Por hacer

.. rubric:: Footnotes

.. [#r1] Para esta documentación se prefiere la palabra ­«Andamio» a la inglesa *Framewok* en tanto considerames que «­Andamio» expresa mucho mejor que la tímipa locución inglesa sobre lo que este tipo de paquetes provee.
